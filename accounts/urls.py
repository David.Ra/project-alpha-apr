from django.urls import path
from accounts.views import show_login, main_logout, main_signup

urlpatterns = [
    path("login/", show_login, name="login"),
    path("logout/", main_logout, name="logout"),
    path("signup/", main_signup, name="signup"),
]
