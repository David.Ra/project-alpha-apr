from django.shortcuts import redirect, render
from accounts.forms import LoginForm, SignupForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User


# Create your views here.
def show_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(
                request,
                username=username,
                password=password,
            )

            if user is not None:
                login(request, user)
                return redirect("list_projects")

    form = LoginForm()
    context = {"form": form}
    return render(request, "accounts/login.html", context)


def main_logout(request):
    logout(request)
    return redirect("login")


def main_signup(request):
    if request.method == "POST":
        signup = SignupForm(request.POST)
        if signup.is_valid():
            username = signup.cleaned_data["username"]
            password = signup.cleaned_data["password"]
            password_confirmation = signup.cleaned_data[
                "password_confirmation"
            ]

            if password == password_confirmation:
                user = User.objects.create_user(username, None, password)
                login(request, user)
                return redirect("list_projects")
            else:
                signup.add_error("password", "Passwords do not match")
    else:
        signup = SignupForm()
    context = {"signup": signup}
    return render(request, "accounts/signup.html", context)
